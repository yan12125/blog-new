---
title: 2013 雲嘉會巧克力傳情 線上訂購系統
id: 92
categories:
  - Uncategorized
tags:
---

放些連結

2013台大雲嘉會 巧心巧語校園傳情
[https://www.facebook.com/chocotaida](https://www.facebook.com/chocotaida "https://www.facebook.com/chocotaida")

線上訂購系統
[http://chyen.twbbs.org/chocolate/](http://chyen.twbbs.org/chocolate/ "http://chyen.twbbs.org/chocolate/")

宣傳影片1
https://www.youtube.com/watch?v=beadA23LfYo

宣傳影片2
https://www.youtube.com/watch?v=EIPXuh9rnkM

原本叫巧克力傳情，but某廠商把這個詞拿去註冊，所以今年不能用巧克力傳情的名字，只能叫巧心巧語，不知對流量有沒有影響？然而，身為一個學生，大可不必管那些商標法的繁文縟節，我稱他為巧克力傳情線上訂購系統，他就是巧克力傳情線上訂購系統，網頁看得到的部份，我叫他巧心巧語，網頁看不到的部份，例如Open graph meta tags, description, keywords等meta tags，我都填巧克力傳情。SEO本來就是一項高深莫測的學問，而線上訂購系統為了達到robustness，犯了一項大忌：幾乎所有內容都是由javascript產生出來的，可能明年要好好改一改，改成用PHP產生。文行至此，只希望兩家廠商的利益糾葛，不要讓學生蒙受其害。

2013/9/29 Update 1
話說根據這篇新聞：
[http://www.tvbs.com.tw/news/news_list.asp?no=miffy011820121012201929](http://www.tvbs.com.tw/news/news_list.asp?no=miffy011820121012201929 "http://www.tvbs.com.tw/news/news_list.asp?no=miffy011820121012201929")
學生不受限制，所以我叫他巧克力傳情，也沒犯法