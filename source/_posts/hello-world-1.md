---
title: Hello world!
id: 1
categories:
  - Uncategorized
date: 2013-04-07 04:13:30
tags:
---

Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!

&nbsp;

It's the second time I install WordPress my my Apache server. The first time is not long ago, but everything seems easier then ever! WordPress is primarily written in PHP, so a little knowledge for PHP is inevitable, but WordPress makes it understandable for non-PHPer. Just some backgrounds of managing server makes everything goes well. For those passionate in sharing ideas but not familiar with tips of manipulating computers, online providers are everywhere. There might be bugs and security issues in such a sophisticated system, but the its popularity and the essence of open source makes bug fixing fast. Another exciting feature of WordPress is that I can worry nothing about permission issues. I just extract the archive to my home directory, thus I'm the common owner of all WordPress files. During the installation, I didn't change the permission or the owner of any files or directories. The only step I've done other than editing wp-config.php is create a new user an the corresponding database on the MySQL server. Yesterday I'm playing around on OwnCloud, a Dropbox-like system, which is also written in PHP. At that scary time, I was almost driven to mad due to recurring permission problems. I did chown and chgrp and chmod again and again, but I was still not satisfied with the result. The primary issue is that Apache server runs as the user www-data, but I hope it has the permission to read and write ONLY the files and directories that I specified for OwnCloud. I've tried apache2-mpm-itk and suPHP, but neither matched my requirement. Other PHP-based systems, like Gallery 3, have similar problem. (Gallery 3 is even worse because the only way to share pictures is uploading. I can't specify a local directory for picture sharing.) These limitations might come from security issues. To prevent attacks, most web applications use their own strict account management system, and WordPress is not an exception. But if they provide an option of integrating local Linux users into these systems, what will occur? The server might be exposed to the threat from invaders, but it's convenient for many personal content providers like me. I know convenience and security can't coexist in most cases, but the problem I faced now is not inconvenience, but not working!
