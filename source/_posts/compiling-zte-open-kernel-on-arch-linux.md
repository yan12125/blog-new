---
title: Compiling ZTE OPEN kernel on Arch Linux
id: 139
categories:
  - Uncategorized
date: 2015-08-12 13:29:53
tags:
---

I need to modify kernel source codes of Firefox OS for my project. Currently I have a ZTE OPEN device (NOTE: it's different from ZTE OPEN C). ZTE have lots of kernel sources released to public, but this device is not included. After several e-mails, ZTE engineers finally release the kernel source for this device. Cheers!

After several days of trials and errors, I finally compiled and installed the kernel image. I use Arch Linux. This distribution features all softwares being latest. Here's an incomplete list of my installed softwares:

*   gcc-multilib 5.2.0-2
*   perl 5.22.0-1
*   python2 2.7.10-1

And here's how I've compiled the kernel:

Now the kernel image is in arch/arm/boot/zImage. The next step is to integrate existing initramfs with the new zImage.

Android/Firefox OS boot.img is a special format. For normal Linux distributions, zImage (vmlinuz) and initrd.gz are the two files necessary for successful booting. On Android/Firefox OS, the two files are integrated into a single file. That is boot.img. The handy tool abootimg can extract and create this file. Here's the steps to generate the new boot.img:

1.  Get the original boot.img. This file can be found in factory images. I use "OPEN(American Standard) SD card upgrading instruction & software package(ebay)-237620B0669OPEN_US_DEV_FFOS_V1.1.0B02(for V1.1)" from [http://www.ztedevice.com/support/smart_phone/b5a2981a-1714-4ac7-89e1-630e93e220f8.html](http://www.ztedevice.com/support/smart_phone/b5a2981a-1714-4ac7-89e1-630e93e220f8.html)
2.  Extract boot.img:
<pre>$ unzip 2014012010173938.zip
$ cd OPEN\(American\ Standard\)\ SD\ card\ upgrading\ instruction\ \&\ software\ package\(ebay\)-237620B0669OPEN_US_DEV_FFOS_V1.1.0B02\(for\ V1.1\)
$ unzip US_DEV_FFOS_V1.1.0B06_UNFUS_SD_For_V1.1.zip boot.img</pre>

3.  Extract initrd from boot.img:
<pre>$ abootimg -x boot.img</pre>

4.  Generate the new boot.img:
<pre>$ abootimg --create boot_new.img -f bootimg.cfg -k ../kernel/arch/arm/boot/zImage -r initrd.img</pre>

Finally, burn the new boot.img into the device. You need adb and fastboot installed.

1.  Enter download mode
<pre>$ adb reboot-bootloader</pre>

2.  Burn the new boot.img
<pre>$ fastboot flash boot boot_new.img && fastboot continue</pre>

Now you can check the new kernel is written:
<pre>$ adb shell cat /proc/version
Linux version 3.0.21 (yen@PC951) (gcc version 4.6.x-google 20120106 (prerelease) (GCC) ) #1 SMP PREEMPT Wed Aug 12 03:29:06 CST 2015
</pre>