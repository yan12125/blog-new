---
title: Placement new
id: 126
categories:
  - Uncategorized
date: 2015-04-21 20:53:39
tags:
---

For advanced C++ programmers, placement new is nothing special. It can initialize a pre-allocated memory block. However, placement new is more than what you think. It accpets an arbitrary number of arguments of arbitrary types.

See the following code snippet:

{% codeblock lang:cpp %}
#include <iostream>
#include <string>
#include <vector>

void* operator new(size_t sz, const std::string& str, const std::vector<char>& vec) noexcept
{
    try
    {
        std::cerr << str << ' ';
        for(char c : vec)
        {
            std::cerr << c;
        }
    }
    catch(...) {}
    return nullptr;
}

int main()
{
    int* p = new ("Hello", {'w', 'o', 'r', 'l', 'd'}) int;
    return 0;
}
{% endcodeblock %}

That means, you can put any possible C++ type as the second argument of placement new operators.

Is it used in real cases? In fact, nothrow new operator in standard C++ utilizes this feature. See the implementation in [libc++](https://github.com/llvm-mirror/libcxx/blob/master/src/new.cpp#L71-88).

Another usage can be found in [libgc](https://github.com/ivmai/bdwgc/blob/master/include/gc_cpp.h#L391-408), a C/C++ garbage collection library.
